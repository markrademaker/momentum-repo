#Package for time/loading management
from time import sleep
from tqdm import tqdm
import pandas as pd
import numpy as np
from load_Data import *

# The main goal of this class is to find the correlation/covariancematrix of all stocks ion the dataframe
# The correlations and covariances are stored as class variables to allow additional function to access the values
# these functions are stil in development
# Author : {Mark Rademaker}
# Edited : 
class Correlations():
    def __init__(self):
        self.CovarianceMatrix, self.CorrelationMatrix, self.codeList = self.load_correlations()

    def load_correlations(self):
        df_cor = pass_query(query_file = "queryCorrelations.txt", b_writemode=False, load_lags=0, load_leads=0, index_name="Fix-Mix48")
        stock = df_cor["Code"] + "." + df_cor["Exchange"]

        df_cor = pd.DataFrame(df_cor)
        del df_cor['AdjustedClose']
        del df_cor['AdjustedCLoseLag']
        del df_cor['Name']

        print(df_cor.columns)
        stocks = df_cor.groupby([stock])
        codeList = []
        k=0
        for name in stocks:
            codeList.append(name[0])
            k+=1
        df_cor.set_index(keys=['date'], drop=False,inplace=True)
        print("Stocks loaded in the current index : ", codeList)

        CovarianceMatrix = [[0.0]*k]*k
        CovarianceMatrix = np.array(CovarianceMatrix)
        CorrelationMatrix = [[0.0]*k]*k
        CorrelationMatrix = np.array(CorrelationMatrix)
        i = 0
        np.set_printoptions(precision = 10, suppress = True)
        print( "---START K LOADING : Calculating covariances and correlations---")

        #Loop over all stocks
        for stockA in codeList:
            j=0
            for stockB in codeList:
                if(stockA != stockB):
                    columnCov = []
                    columnCov = (stocks.get_group(stockA).get('Change') - stocks.get_group(stockA).get('Change').dropna().mean()) * (stocks.get_group(stockB).get('Change')-stocks.get_group(stockB).get('Change').dropna().mean())
                    #Calculating Covariance
                    CovarianceMatrix[i][j] = columnCov.dropna().sum()/(columnCov.dropna().count()-1)
                    #Calculating Correlations
                    CorrelationMatrix[i][j] = (columnCov.dropna().sum()/(columnCov.dropna().count()-1))/ (stocks.get_group(stockA).get('Change').dropna().std() * stocks.get_group(stockB).get('Change').dropna().std())
                j+=1
            i+=1
        print("---LOADING COMPLETED : Covariances & Correlations loaded---")
        return CovarianceMatrix, CorrelationMatrix, codeList
    
    def get_correlations(self,stockA, stockB):
    # Type the stocks in the panel above when running code
        print("Finding covariance of : ", stockA, stockB , "...")
        a = self.codeList.index(stockA.upper())
        b = self.codeList.index(stockB.upper())
        print("Covariance = ", self.CovarianceMatrix[a][b])
        print("Correlations  = ", self.CorrelationMatrix[a][b])

    # Gives Covariances of Long positions, Covariances of Short positions.
    def get_position_correlations(self,input_long, input_short, threshold):
        ar_rejected = []
        count = 0
        b_rejected = False

        for stocka in input_long[:-1]:
            count+=1
            for stockb in input_long[count:]:
                if(stocka!=stockb):
                    a = self.codeList.index(stocka.upper())
                    b = self.codeList.index(stockb.upper())
                    if(self.CorrelationMatrix[a][b]> threshold):
                        b_rejected = True
                        pair = stocka + " " + stockb
                        if(not(stockb in ar_rejected)): 
                            ar_rejected.append(stockb)
        for stocka in input_short:
            for stockb in input_short:
                if(stocka!=stockb):
                    a = self.codeList.index(stocka.upper())
                    b = self.codeList.index(stockb.upper())
                    if(self.CorrelationMatrix[a][b]>threshold):
                        pair = stocka + " " + stockb
                        b_rejected = True
                        if(not(stockb in ar_rejected)): 
                            ar_rejected.append(stockb) 
        return b_rejected, ar_rejected

    # Long-Short Pair Covariance , end inserting pairs by typing end
    #threshold = float(input("Enter threshold value..."))
    def get_long_short_correlations(self,list_pair, threshold):
        threshold = 0.5
        rejected_pair=[]
        input_pair = ""
        while input_pair != 'end':
            input_pair = input("Enter a Short-Long pair stocks separated by space ....")
            list_pair.append(input_pair.split(" "))
        list_pair.pop()
        print("Long-Short covariances : ")
        print(list_pair)
        for pair in list_pair:
                if(pair[0]!=pair[1]):
                    a = self.codeList.index(pair[0].upper())
                    b = self.codeList.index(pair[1].upper())
                    print("Correlation ", pair[0], " ", pair[1], "= ", self.CorrelationMatrix[a][b])
                    if(self.CorrelationMatrix[a][b] > threshold):
                        pair = pair[0] + " " + pair[1]
                        rejected_pair.append(pair)
        print("Rejected pairs : ", rejected_pair)
    
    def get_CovarianceMatrix(self):
        return self.CovarianceMatrix

    def get_CorrelationsMatrix(self):
        return self.CorrelationMatrix


