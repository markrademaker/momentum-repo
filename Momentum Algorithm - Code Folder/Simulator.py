## Factor Orange Capital Backtest File Python
## This functions is the backtest simulator, it simulates the performance of an algorithm with the passed settings as arguments
## over the time period of df. The class variables contain information regarding the performance of the algorithm and the settings
##
## Author : {Max Saverkoul, Bas Peeters}
## Edited : {Mark Rademkaer}
##

# All  packages needed for the entire code are installed 
# When some packages do not work, restart visual studio code as administrator

#Code for correlations
import Correlations

#Package to connect to SQL
#import pyodbc

#Package used for easy to use mathematical operations
import numpy as np

#Package for data manipulation
import pandas as pd

#package for variance
from statistics import variance

#Package used for plotting
import matplotlib.pyplot as plt

#Package used to check runtime/datetime
from datetime import datetime
import time
import tqdm
from tqdm import tqdm

#Package for various machine learning models
import sklearn
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LogisticRegression # Imports the logistic regression model
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler

from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelBinarizer # Used for one hot encoding

#Packages for machine learning model score metrics
from sklearn.metrics import accuracy_score # Used for the accuracy score metric
from sklearn.metrics import precision_score # Used for the precision score metric
from sklearn.metrics import recall_score # Used for the recall score metric
from sklearn.metrics import confusion_matrix # Used for the confusion matrix
from sklearn.metrics import plot_confusion_matrix # Used for the plot of the confusion matrix
from sklearn.metrics import f1_score # Used for the f1 score metric
from sklearn.metrics import roc_curve, auc # Used for the roc_curve
from sklearn.metrics import mean_squared_error, r2_score

#import graphviz # Used for plotting a decision tree

#Package Mutes sklearn warnings
from warnings import simplefilter
simplefilter(action='ignore', category=FutureWarning)
simplefilter(action='ignore', category=DeprecationWarning)

from sklearn.tree import DecisionTreeClassifier, export_graphviz # Import Decision Tree Classifier
from six import StringIO
from IPython.display import Image

class Simulator:
    def __init__(self, name, df, b_compounding,b_regression, v_pairs, v_trainingDepth, v_trading_costs, b_MOP, b_plot_index, b_StopLoss,
                v_stop_loss_perc_long, v_stop_loss_perc_short, n_begin_backtest,n_end_backtest,
                b_exclude_div, b_exclude_earning,  b_exclude_splits, b_exclude_highcorrelations, o_loaded_cor,
                v_correlation_threshold,b_selectionday_outlier, v_selectionday_outlier, b_return_withcosts):

        self.name = name
        #Dataframe
        self.df = df

        #Summary statistics
        
        self.b_regression = b_regression
        self.trading_day = 0
        self.v_stoploss_hits = 0
        self.v_trades_done = 0
        self.v_long_correct = 0
        self.v_short_correct = 0
        self.v_total_ret_long= 0
        self.v_total_ret_short= 0
        self.v_start_i = 0
        self.v_correlation_rej = 0
        self.v_no_trade = 0
        self.v_balance_withcosts = 0 #Starting balance with trading costs
        self.v_balance = 0 #Starting balance without trading costs
        self.v_trading_costs = v_trading_costs
        self.v_sharpe = -100

        #Initialize balances, this will become an array with all daily portfolio returns
        self.ar_balance_history_withcosts = np.array([])
        self.ar_balance_history = np.array([])
        self.ar_balance_history_reg = np.array([])
        self.ar_returns_history = np.array([])

        #Stock counter
        self.ar_long_stocks_history = np.array([])
        self.ar_short_stocks_history = np.array([])

        #Plot 
        self.b_plot_index = b_plot_index
        self.b_return_withcosts = b_return_withcosts

        #SETTINGS
        self.v_pairs= v_pairs
        #self.n_days = n_days
        self.n_begin_backtest = n_begin_backtest + " 00:00:00"
        self.n_end_backtest = n_end_backtest
        self.v_trainingDepth = v_trainingDepth
        self.b_compounding = b_compounding

        #exclude/include calendar events
        self.b_exclude_div = b_exclude_div
        self.b_exclude_earning = b_exclude_earning
        self.b_exclude_splits = b_exclude_splits

        #correlation correction
        self.o_loaded_cor = o_loaded_cor
        self.b_exclude_highcorrelations = b_exclude_highcorrelations
        self.v_correlation_threshold = v_correlation_threshold

        #selectionday outlier
        self.b_selectionday_outlier = b_selectionday_outlier
        self.v_selectionday_outlier = v_selectionday_outlier
        
        #stoploss
        self.b_stoploss = b_StopLoss
        self.v_stop_loss_perc_long = v_stop_loss_perc_long
        self.v_stop_loss_perc_short = v_stop_loss_perc_short

        #mop
        self.b_MOP = b_MOP
        self.v_MOP_threshold_min = 0.01 #if min is more than thres, no trade
        self.v_MOP_threshold_max = -0.01 #if max is less than thres, no trade
        self.v_MOP_count = 0
        self.v_MOP_return = 0

        #Draw down
        self.v_min_day = 100
        self.v_min_month = 100
        self.v_min_year = 100

        #Start and end iteration of backtest
        self.n_start = None
        self.n_end = None






    #Simulate Backtest Method
    def Simulate(self):
        
        #Creates a frame for every day
        self.df = self.df.sort_values("BarDate")
        df_days = self.df.groupby("BarDate")

        #Gets unique names for every day
        ser_days_names = df_days.groups.keys()

        #Index return
        if self.b_plot_index:
            ar_index_history = np.array([])
            v_index_balance = 0

        #Get start date of back_test
        ts_start_timestamp = pd.Timestamp(self.n_begin_backtest)

        #Print date out of range when error is thrown
        try:
            self.n_start = list(ser_days_names).index(ts_start_timestamp)
        except ValueError: 
            print("No data from this starting day, data starts at : ", list(ser_days_names)[0], ", also weekend days / holidays not in data")
    	
        #Get end date of backtest
        if(not(self.n_end_backtest == "now")):
            self.n_end_backtest += " 00:00:00"
            ts_start_timestamp = pd.Timestamp(self.n_begin_backtest)
            self.n_end = list(ser_days_names).index(ts_start_timestamp)
        else: 
            self.n_end = len(list(ser_days_names))
        print(self.n_end)
        
        #Simulate
        #tqdm is loading icon
        for i, day_name in tqdm(enumerate(list(ser_days_names)[self.n_start:self.n_end])):
            
            self.trading_day+=1

            if df_days.get_group(day_name).shape[0] >= 2*self.v_pairs:                                #and (self.n_days == 'all' or i>len(ser_days_names)-self.n_days): 
                
                #Retrieves the frame of the group corresponding to the day of day_name
                daily_df = df_days.get_group(day_name)

                #Remove stock from options if this is earnings release day
                if(self.b_exclude_earning):
                    daily_df = daily_df.drop(daily_df[daily_df.Earnings_announcement_ind == 1].index)
                    daily_df = daily_df.drop(daily_df[daily_df.Earnings_announcement_ind_NextDay == 1].index)

                #Remove stock from options if this stock has a split on that day
                if(self.b_exclude_splits):
                    daily_df = daily_df.drop(daily_df[daily_df.split_ind == 1].index)
                    daily_df = daily_df.drop(daily_df[daily_df.split_ind_NextDay == 1].index)

                #Remove stock from options if this stock hands out dividend on that day
                if(self.b_exclude_div):
                    daily_df = daily_df.drop(daily_df[daily_df.dividend_ind ==1].index )
                    daily_df = daily_df.drop(daily_df[daily_df.dividend_ind_NextDay == 1].index)

                #Loads data of trainingsdepth    
                ser_mom_PastDaysGrowth, v_min, v_max, v_all_avg = self.trainings_depth(daily_df)

                #exclude highly correlated stocks
                if(self.b_exclude_highcorrelations):
                    ser_mom_PastDaysGrowth, daily_df = self.exclude_correlations(daily_df, ser_mom_PastDaysGrowth)

                #Trading day outlier
                if(self.b_selectionday_outlier):
                    ar_ind_up, ar_ind_down, v_pairs_traded = self.selectionday_outlier(daily_df, ser_mom_PastDaysGrowth)
                    #Go to next iteration if no pairs can be traded
                    if ( v_pairs_traded == 0 ):
                        continue

                else:
                    #Choose best and worst V_pairs stocks without outlier filter
                    ar_ind_up = np.argpartition(ser_mom_PastDaysGrowth, -self.v_pairs)[-self.v_pairs:]
                    ar_ind_down = np.argpartition(ser_mom_PastDaysGrowth, self.v_pairs)[:self.v_pairs]
                    v_pairs_traded = self.v_pairs

                #Get chosen stocks
                ser_long_stocks_chosen = daily_df.iloc[ar_ind_up]
                ser_short_stocks_chosen = daily_df.iloc[ar_ind_down]
                
                #Calculate profit and loss of this day
                v_return_long, v_return_short = self.calculate_daily_pnl(ser_long_stocks_chosen, ser_short_stocks_chosen,v_pairs_traded)

                #MOP : No trading in market skewed day
                if self.b_MOP and (v_min > self.v_MOP_threshold_min or v_max < self.v_MOP_threshold_max):
                    v_return_long, v_return_short = self.MOP(v_return_long, v_return_short)

                #Save results of day
                self.ar_returns_history = np.append(self.ar_returns_history, v_return_long+v_return_short)

                #Save amount of times stock is chosen long and short
                ser_long_stocks= daily_df["SymbolExchangeCode"].iloc[ar_ind_up]
                ser_short_stocks= daily_df["SymbolExchangeCode"].iloc[ar_ind_down]

                #Save results of day long and short
                self.ar_long_stocks_history = np.append(self.ar_long_stocks_history, ser_long_stocks)
                self.ar_short_stocks_history = np.append(self.ar_short_stocks_history, ser_short_stocks)

                #Save statistics
                ar_bar_long_stocks, ar_bar_long_counts = np.unique(self.ar_long_stocks_history, return_counts=True)
                ar_bar_short_stocks, ar_bar_short_counts = np.unique(self.ar_short_stocks_history, return_counts=True)
                    
                #Adjust Summary statistics
                self.v_trades_done += 2*v_pairs_traded
                self.v_long_correct += (daily_df["NextDayGrowth"].iloc[ar_ind_up]>0).sum()
                self.v_short_correct += (daily_df["NextDayGrowth"].iloc[ar_ind_down]<0).sum()
                self.v_total_ret_long += v_return_long
                self.v_total_ret_short += v_return_short

                #Update balance
                self.adjust_balance(v_return_long, v_return_short)

                #Index
                if self.b_plot_index:
                    v_index_balance = v_index_balance + daily_df["TotPastDayGrowth"].mean()
                    ar_index_history = np.append(ar_index_history, v_index_balance)
                

                # Calculates the max draw down per year, per month and per day
                if(self.trading_day%252 == 0):
                    #On average 250 trading days per year
                    self.v_min_year = min((self.ar_balance_history[-1] - self.ar_balance_history[-250]),self.v_min_year)
                if(self.trading_day%21 == 0):
                    #On average 21 trading days per month
                    self.v_min_month = min((self.ar_balance_history[-1] - self.ar_balance_history[-21]),self.v_min_month)
                elif(self.trading_day > 1):
                    try:
                        self.v_min_day= min((self.ar_balance_history[-1] - self.ar_balance_history[-2]),self.v_min_day)
                    except IndexError:
                        self.v_min_day= self.ar_balance_history[-1]
                
        #--------------------------------- END OF ITERATIONS -------------------------------------
        
        #Sharpe (NOT FINISHED)
        #v_risk_free_rate = 0.01 #As risk free rate the average US treasury bond yield should be taken, 1% is a guess
        v_std_dev = np.std(self.ar_returns_history)
        v_return_rate = (self.ar_balance_history[-1]) / 11
        self.v_sharpe = (v_return_rate)/v_std_dev

        #master_backtest stats
        v_return = round(100*(self.ar_balance_history[-1]),2)
        if self.b_return_withcosts:
            v_return_withcosts =  round(100*(self.ar_balance_history_withcosts[-1]),2)      
        
        return 





    #Function plots figure with results
    def plot_figure(self):
        
        #Plot the figure    
        plt.figure(figsize=(30, 12))
        plt.plot(self.ar_balance_history)
        
        if self.b_return_withcosts:
            plt.plot(self.ar_balance_history_withcosts)

        plt.plot(self.ar_balance_history_reg)
        if self.b_plot_index: 
                plt.plot(self.ar_index_history)

        plt.legend(['Momentum minus Trading Costs', 'Momentum', 'Index Average'])
        plt.ylabel('Balance')
        plt.xlabel('Days')
        plt.grid(True)
        plt.title('Simulation P/L')
        plt.show()
        return

    #Function for determining trainingsdepth of backtest

    def trainings_depth(self, daily_df):
        #Total growth from 'TrainingDepth'-1 days ago until today 
        str_column_name = "TotPast" + str(self.v_trainingDepth+1) + "DayGrowth"
        if self.v_trainingDepth == 0:
            ser_mom_PastDaysGrowth = daily_df["TotPastDayGrowth"]
            v_min = daily_df["TotPastDayGrowth"].min()
            v_max = daily_df["TotPastDayGrowth"].max()
            v_all_avg = daily_df["TotPastDayGrowth"].mean()
        else:
            ser_mom_PastDaysGrowth = daily_df[str_column_name]
            v_min = daily_df[str_column_name].min()
            v_max = daily_df[str_column_name].max()
            v_all_avg = daily_df[str_column_name].mean()

        return ser_mom_PastDaysGrowth, v_min, v_max, v_all_avg

    #Function updates the balance for PnL on a day in the simulation
    def adjust_balance(self, v_return_long, v_return_short):

        #Adds the daily portfolio return to the balance with trading costs (balance) and the balance without trading costs (balance_nocosts)
        #With compounding
        if self.b_compounding:

            #Adjust current balance
            self.v_balance_withcosts = self.v_balance_withcosts * (1 + v_return_long + v_return_short - self.v_trading_costs)
            self.v_balance = self.v_balance * (1 + v_return_long + v_return_short)

            #Fills the array with daily portfolio returns
            self.ar_balance_history = np.append(self.ar_balance_history, self.v_balance)
            
            if self.b_return_withcosts:
                self.ar_balance_history_withcosts = np.append(self.ar_balance_history_withcosts, self.v_balance_withcosts)
            
        #Without compounding
        else:
            self.v_balance_withcosts = self.v_balance_withcosts + v_return_long + v_return_short - self.v_trading_costs
            self.v_balance = self.v_balance + v_return_long + v_return_short 

            #Fills the array with daily portfolio returns
            self.ar_balance_history = np.append(self.ar_balance_history, self.v_balance)
            
            if self.b_return_withcosts:
                self.ar_balance_history_withcosts = np.append(self.ar_balance_history_withcosts, self.v_balance_withcosts)
        return

    #Function calculates the PnL of that day
    def calculate_daily_pnl(self, ser_long_stocks_chosen, ser_short_stocks_chosen, v_pairs_traded):

        #Profit and loss with stop loss
        if self.b_stoploss:
            
            #Avoids small error: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
            ser_long_stocks_chosen_c = ser_long_stocks_chosen.copy()
            ser_short_stocks_chosen_c = ser_short_stocks_chosen.copy()

            ser_short_stocks_chosen_c["Stop Short"] = ser_short_stocks_chosen["AdjustedClose"]*(1+self.v_stop_loss_perc_short/100)
            ser_long_stocks_chosen_c["Stop Long"] = ser_long_stocks_chosen["AdjustedClose"]*(1-self.v_stop_loss_perc_long/100)

            #Calls function to calculate PnL with stoploss
            #If 0 stocks chosen return = 0
            if(len(ser_long_stocks_chosen) == 0):
                ser_short_stocks_chosen_c["SL Return"] =0
                ser_long_stocks_chosen_c["SL Return"] =0

                v_return_long = 0
                v_return_short = 0
                
            else:
                ser_short_stocks_chosen_c["SL Return"] =ser_short_stocks_chosen_c.apply(self.sl_return_short, axis=1)
                ser_long_stocks_chosen_c["SL Return"] = ser_long_stocks_chosen_c.apply(self.sl_return_long, axis=1)

                v_return_long = ser_long_stocks_chosen_c["SL Return"].sum()/(v_pairs_traded * 2)
                v_return_short = -ser_short_stocks_chosen_c["SL Return"].sum()/(v_pairs_traded * 2)

        #Profit and loss of trading day without stop loss
        else:
            #Returns the the sum of all returns of chosen stocks divided by the number of stocks, so the daily portfolio return
            v_return_long = ser_long_stocks_chosen["NextDayGrowth"].sum()/(v_pairs_traded*2)
            v_return_short = -ser_short_stocks_chosen["NextDayGrowth"].sum()/(v_pairs_traded*2)

        return v_return_long, v_return_short

    #Function filters out stocks exceeding outlier threshold
    def selectionday_outlier(self, daily_df, ser_mom_PastDaysGrowth):

        #Determine outliers on selectionday
        outlier_indicator_up = daily_df.where(daily_df["TotPastDayGrowth"]>-self.v_selectionday_outlier).dropna()
        outlier_indicator_down = daily_df.where(daily_df["TotPastDayGrowth"]<self.v_selectionday_outlier).dropna()

        #Filter out outliers
        filter_up = ser_mom_PastDaysGrowth.index.isin(outlier_indicator_up.index)
        filter_down = ser_mom_PastDaysGrowth.index.isin(outlier_indicator_down.index)

        ser_mom_PastDaysGrowth_up = ser_mom_PastDaysGrowth.where(filter_up)
        ser_mom_PastDaysGrowth_down = ser_mom_PastDaysGrowth.where(filter_down)

        v_nan = ser_mom_PastDaysGrowth_up.isnull().sum(axis = 0)
        
        #If less than 5 stocks are available on either side
        if(len(ser_mom_PastDaysGrowth_up) - v_nan < self.v_pairs or len(ser_mom_PastDaysGrowth_down) - v_nan < self.v_pairs):
            v_pairs_limited = min(len(ser_mom_PastDaysGrowth_up)-v_nan, len(ser_mom_PastDaysGrowth_down)-v_nan)
            if(v_pairs_limited == 0):
                self.v_no_trade+=1
                return [0],[0], 0
            
            ar_ind_up = np.argpartition(ser_mom_PastDaysGrowth_up,range(len(ser_mom_PastDaysGrowth_up)))[-v_nan - v_pairs_limited:-v_nan] 
            ar_ind_down = np.argpartition(ser_mom_PastDaysGrowth_down, range(len(ser_mom_PastDaysGrowth_up)))[:v_pairs_limited]
        else:
            v_pairs_limited = self.v_pairs
            #NaN are placed at bottom of partition

            ar_ind_up = np.argpartition(ser_mom_PastDaysGrowth_up,range(len(ser_mom_PastDaysGrowth_up)))[-v_nan - v_pairs_limited:-v_nan] 
            ar_ind_down = np.argpartition(ser_mom_PastDaysGrowth_down, range(len(ser_mom_PastDaysGrowth_up)))[:v_pairs_limited]
        
        return ar_ind_up, ar_ind_down, v_pairs_limited

    #Function exclude stocks that are highly correlated from the same position
    def exclude_correlations(self, daily_df, ser_mom_PastDaysGrowth):
        #Find the 5 highest and lowest stocks
        b_rejected = True
        #Find if those 5 are not correlated too much
        while(b_rejected == True):
            #Check Correlations and if a pair exceeds threshold
            b_rejected, ar_rejected = self.o_loaded_cor.get_position_correlations(daily_df.iloc[np.argpartition(ser_mom_PastDaysGrowth, -self.v_pairs)[-self.v_pairs:]]["SymbolExchangeCode"],
                                                                                                    daily_df.iloc[np.argpartition(ser_mom_PastDaysGrowth, self.v_pairs)[:self.v_pairs]]["SymbolExchangeCode"], 
                                                                                                        self.v_correlation_threshold)
            if(b_rejected):
                for stock in ar_rejected:
                    #Remove 2nd stock from pair from the set of options'
                    self.v_correlation_rej += 1
                    v_index = np.where(daily_df["SymbolExchangeCode"]==stock)
                    daily_df.drop(index = ser_mom_PastDaysGrowth.iloc[v_index].index.tolist(), inplace = True)
                    ser_mom_PastDaysGrowth.drop(index = ser_mom_PastDaysGrowth.iloc[v_index].index.tolist(), inplace = True)

        return ser_mom_PastDaysGrowth, daily_df
    
    #Function ensures no trading is done when market is lopsided
    def MOP(self, v_return_long, v_return_short):
        self.v_MOP_return -= (v_return_long + v_return_short)
        v_return_short=0
        v_return_long=0
        self.v_MOP_count +=1
        return  0, 0

    # This function is used to make a bar chart to see how often each stock is chosen
    def sl_return_short(self,df):
        if (df["Stop Short"] < df["NextAdjustedOpen"]):

            #self.v_stoploss_hits += df.where(df["Stop Short"]<df["NextAdjustedOpen"]).count()

            return (df["NextAdjustedOpen"]-df["AdjustedClose"])/df["AdjustedClose"]
        elif (df["Stop Short"] < df["NextAdjustedHigh"]):

            #self.v_stoploss_hits += df.where(df["Stop Short"]<df["NextAdjustedHigh"]).count()

            return (df["Stop Short"]-df["AdjustedClose"])/df["AdjustedClose"]
        else:
            return df["NextDayGrowth"]
        return

    def sl_return_long(self,df):
        if (df["Stop Long"] > df["NextAdjustedOpen"]): 

            #self.v_stoploss_hits += df.where(df["Stop Long"]>df["NextAdjustedOpen"]).count()

            return (df["NextAdjustedOpen"]-df["AdjustedClose"])/df["AdjustedClose"]
        elif (df["Stop Long"] > df["NextAdjustedLow"]):

            #self.v_stoploss_hits += df.where(df["Stop Long"]>df["NextAdjustedLow"]).count()

            return (df["Stop Long"]-df["AdjustedClose"])/df["AdjustedClose"]
        else:
            return df["NextDayGrowth"]
        return

    def show_counts(self,ar_bar_long_stocks, ar_bar_long_counts, ar_bar_short_stocks, ar_bar_short_counts):
        
        #Plot the bar diagram for long and short stock count
        plt.rcdefaults()
        fig, ax = plt.subplots()
        #y_pos = np.arange(len(bar_long_stocks))
        plt.figure(figsize=(10,30))
        ax.barh(ar_bar_long_stocks, ar_bar_long_counts)
        #ax.set_yticks(y_pos, unique)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Chosen to go long')
        ax.set_title('How many times is the stock picked?')
        plt.show()

        plt.rcdefaults()
        fig, ax = plt.subplots()
        ax.barh(ar_bar_short_stocks, ar_bar_short_counts)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_xlabel('Chosen to go short')
        ax.set_title('How many times is the stock picked?')
        plt.show()
        return

    # This function compares the total return of multiple algorithms over a certain lookback period. Then it chooses the algorithm with highest returns over this lookbackperiod for the next day

    def VirtualBetPredictor(self,df_balance_histories, v_lookbackperiod, df_returns_histories, ar_algorithm_names):

            ar_balance_history_regictor = []
            v_virtual_bet_predictor_result = 0

            #Creates a dataframe with the total returns over the lookbackperiod
            df_lookbackperiod_return = df_balance_histories.diff(axis = 0, periods = v_lookbackperiod)
            #df_lookbackperiod_return = df_balance_histories.rolling(v_lookbackperiod).mean()
            #iterate over all rows in df_lookbackperiod_return
            for j in range(df_lookbackperiod_return.shape[0]):
                    if v_lookbackperiod <= j < df_lookbackperiod_return.shape[0]-1:
                            #iterate over all columns in df_lookbackperiod_return
                            for i in range(df_lookbackperiod_return.shape[1]):
                            #Checks which algorithm has the highest return over the lookback period 
                                    if df_lookbackperiod_return.iloc[j,i] == df_lookbackperiod_return.max(axis=1).iloc[j]:
                                            #Chooses the algorithm with the highest return over the lookback period for the next day
                                            v_virtual_bet_predictor_result = v_virtual_bet_predictor_result + df_returns_histories.iloc[j+1,i]

                                            #Adds the value of the next day return of the 'best strategy' to the ar_balance_history_regictor
                                            ar_balance_history_regictor = np.append(ar_balance_history_regictor, v_virtual_bet_predictor_result)
                    else:
                            #As we cannot find the best algorithm before we can construct a rolling sum
                            #We will simply follow the first algorithm up until then
                            v_virtual_bet_predictor_result = v_virtual_bet_predictor_result + df_returns_histories.iloc[j,0]

                            #Adds the value of the next day return of the 'best strategy' to the ar_balance_history_regictor
                            ar_balance_history_regictor = np.append(ar_balance_history_regictor, v_virtual_bet_predictor_result)

            #Plots the returns of the figures with different settings  
            plt.figure(figsize=(40, 10))
            plt.plot(df_balance_histories)
            plt.plot(ar_balance_history_regictor)
            ar_legends = np.append(ar_algorithm_names, 'Predictor')
            plt.legend(ar_legends) 
            plt.ylabel('Return')
            plt.xlabel('Days')
            plt.grid(True)
            plt.title('Simulation P/L')
            plt.show()

            return ar_balance_history_regictor








#Get functions
    #Get returns of backtest 
    def get_ar_returns_history(self):
        return self.ar_returns_history
    
    #Get balance of backtest with costs
    def get_ar_balance_history_withcosts(self):
        if self.b_return_withcosts:
            return self.ar_balance_history_withcosts
    
    #Get balance of backtest
    def get_ar_balance_history(self): 
        return self.ar_balance_history
    
   
    #Prints settings of backtest
    def get_settings(self):
        print("The following settings have been used :")
        print("Compounding : ", self.b_compounding)
        print("Pairs : ", self.v_pairs)
        print("Exclude_div : ", self.b_exclude_div)
        print("Exclude_earning : ", self.b_exclude_earning)
        print("Exclude_splits : ", self.b_exclude_splits )
        print("TrainingDepth : ", self.v_trainingDepth)
        print("Exclude_highcorrelations : ", self.b_exclude_highcorrelations, " |  with values above : ", self.v_correlation_threshold)
        print("Stoploss used : ", self.b_stoploss, " | with value long: ", self.v_stop_loss_perc_long, " | and value short : ", self.v_stop_loss_perc_short)
        print("b_mop used : ", self.b_MOP)
        print("Trading day outliers :", self.b_selectionday_outlier, "  | with values above : ", self.v_selectionday_outlier)
    pass

    #Prints summary statistics of backtest
    def get_summary(self):
        #_____________________________________________________________________________________________________
        #Summary
        print("______________________***TRADE SUMMARY***_______________________")
        print("Profit: +", round(100*(self.ar_balance_history[-1]),2), "%")
        print("")
        print("Number of trading days: ", self.trading_day-self.v_start_i)
        print("Number of trades: ", self.v_trades_done)
        print("Number of days no trades because of outliers ", self.v_no_trade)
        print("Number of Stoploss hits : ", self.v_stoploss_hits)
        print("Accuracy: ", round(100*(self.v_short_correct + self.v_long_correct) / (self.v_trades_done),2), "%")
        print("")
        print("Long correct: ", self.v_long_correct, "(", round(100 * self.v_long_correct / (self.v_trades_done / 2),2), "%)")
        print("Short correct: ", self.v_short_correct, "(", round(100 * self.v_short_correct / ( self.v_trades_done / 2),2), "%)")
        print("Average return long: ", round(100*(self.v_total_ret_long / (self.v_trades_done / 2)),4), "%"    )
        print("Average return short: ", round(100*(self.v_total_ret_short/(self.v_trades_done / 2)),4), "%"    )
        print("")
        print("Volatility: ", round(variance(self.ar_returns_history),2))
        print("Sharpe Ratio: ", round(self.v_sharpe,3))
        print("Max drawdown per year:  ", self.v_min_year, "  Max drawdown per month:  ", self.v_min_month, "  Max drawdown per day:  ", self.v_min_day)
        if(self.b_exclude_highcorrelations):
            print( "Number of rejected trades because of correlations : ", self.v_correlation_rej)
        print("")
        #Linear Regression
        if(self.b_regression):
            regr = LinearRegression()
            ar_index = np.arange(len(self.ar_balance_history))
            ar_index = ar_index.reshape(-1, 1)
            regr.fit(ar_index, self.ar_balance_history)
            self.ar_balance_history_reg = regr.predict(ar_index)
            print("Linear Regession Results : ")
            print("Coefficient: %.6f" % regr.coef_)
            print("Mean squared error: %.3f" % mean_squared_error(self.ar_balance_history, self.ar_balance_history_reg))
            print("R-squared: %.3f" % r2_score(self.ar_balance_history, self.ar_balance_history_reg))
            print("")
        if self.b_return_withcosts:
            print("Result with costs: +", round(100*    (self.ar_balance_history_withcosts[-1])    ,2), "%")
        print(" ")
        #Results with MOP
        if self.b_MOP:
            print("MOP Used:", self.v_MOP_count, "days which resulted in a ", round(100*self.v_MOP_return,4), "% P/L")   
        print("")
        print("________________________________________________________________")
        print("")
        #_____________________________________________________________________________________________________
        pass 


    def get_name(self):
        return self.name

    def get_drawdown_year(self):
        return self.v_min_year
        
    def get_drawdown_month(self):
        return self.v_min_month
    
    def get_drawdown_day(self):
        return self.v_min_day