
# Data is imported from SQL with a query
# Dit is een verbinding naar de lokale SQL, niet naar de server.
#Package to connect to SQL
import pyodbc
import pandas as pd
import tqdm
def pass_query(query_file, b_writemode, load_lags, load_leads, index_name):
    #Package to connect to SQL
    import pyodbc
    import pandas as pd
    import tqdm

    print("loading database ...")

    cnxn = pyodbc.connect("DRIVER={ODBC Driver 17 for SQL Server};"

                            "Server=10.0.1.6\MAMSTUDIO_DEV;"

                            "Database=MAM.Factor.Farm;"

                            "uid=mamma;pwd=mamma")

    

    # Gets the amount of lagged days desired
    with open (query_file, "r") as f:
        contents = f.readlines()
    
    #for line in contents:
    #    line.replace("index_here", str(index_name))
    
    v_write = 2
    if(b_writemode):
        for i in range(load_leads,1,-1):
            str_lead = "LEAD(eod.Bar.[AdjustedClose],"+ str(i) + ") OVER (PARTITION BY eod.bar.SymbolId ORDER BY eod.Bar.SymbolId, eod.Bar.[Date]) Next"+ str(i) +"AdjustedClose,\n"
            contents.insert(v_write, str_lead)
            v_write+=1

        v_write+=6
        for j in range(load_lags,1,-1):
            str_lag = "LAG(eod.Bar.[AdjustedClose], "+str(j)+") OVER (PARTITION BY eod.bar.SymbolId ORDER BY eod.Bar.SymbolId, eod.Bar.[Date]) Previous"+ str(j) +"AdjustedClose,\n"
            contents.insert(v_write, str_lag)
            v_write+=1

        with open(query_file, "w") as f:
            contents = "".join(contents)
            f.write(contents)

    with open (query_file, "r") as file:
        data=file.read().replace('\n', ' ')
    df = pd.read_sql_query(data, con=cnxn)

    return df
    
def load_EOD(stock_index, security_type):
    import tqdm
    df = None

    #File type
    str_format = "json"

    #Creates a df for each individual stock and concatenates it to the other stock df's 
    for stock in stock_index:
        print(stock)
        #Creates the URL/API
        str_url_API = "https://eodhistoricaldata.com/api/eod/"+ stock +"?api_token=5ffd9842655256.72432671&from=2010-04-01&fmt="+str_format

        #Makes a dataframe from the json file
        df_stock = pd.read_json(str_url_API)

        #Adds SymbolExchangeCode to the dataframe
        if(df_stock.size > 0):
            df_stock.insert(1,"SymbolExchangeCode", stock)

        if (security_type == "stock"):
            df_stock = df_stock.rename(columns={"adjusted_close":"AdjustedClose"}) 
        elif (security_type == "crypto"):
            df_stock = df_stock.rename(columns={"adjusted_close":"AdjustedClose"}) 
        
        df_stock = df_stock.rename(columns={"date":"BarDate"}) 

        df_stock["PreviousAdjustedClose"] = df_stock["AdjustedClose"].shift(1, axis = 0)
        df_stock["Previous2AdjustedClose"] = df_stock["AdjustedClose"].shift(2, axis = 0)
        df_stock["Previous3AdjustedClose"] = df_stock["AdjustedClose"].shift(3, axis = 0)
        df_stock["Previous4AdjustedClose"] = df_stock["AdjustedClose"].shift(4, axis = 0)
        df_stock["Previous5AdjustedClose"] = df_stock["AdjustedClose"].shift(5, axis = 0)
        df_stock["Previous6AdjustedClose"] = df_stock["AdjustedClose"].shift(6, axis = 0)
        df_stock["NextAdjustedClose"] = df_stock["AdjustedClose"].shift(-1, axis = 0)

        df = pd.concat([df, df_stock], ignore_index=True)

    return df
# Code adds past(s) day growth to the database

def load_new_variables(df, days_growth):
    #Check for NaN
    df.isnull().sum()

    #Drops rows with NaN and checks if all rows with NaN are deleted
    df = df.dropna()
    df.isnull().sum()

    #Checks the types of data
    df.info()

    #Look at the min, max and other characteristics
    df.describe()

    #Orders the df by date
    df = df.sort_values("BarDate")

    #Adds the weekday as index to the df, 0=monday, 1=tuesday, 2=wednesday, 3=thursday, 4=Friday
    if(days_growth>1):
        df["PastDayGrowth"] = (df["AdjustedClose"] - df["PreviousAdjustedClose"]) / df["PreviousAdjustedClose"] 
        df["Past2DayGrowth"] = (df["PreviousAdjustedClose"] - df["Previous2AdjustedClose"]) / df["Previous2AdjustedClose"]

        #Total growth over the past 1, 2 ,3, 4 & 5 days
        df["TotPastDayGrowth"] = df["PastDayGrowth"]
        df["TotPast2DayGrowth"] = df["PastDayGrowth"] +  df["Past2DayGrowth"] 

    #Daily stock growth 0, 1, 2 & 3 days ago
    for i in range(3,days_growth+1):
        str_name_tot = "TotPast"+ str(i) + "DayGrowth"
        df[str_name_tot] =  df["PastDayGrowth"] +  df["Past2DayGrowth"] 
        for j in range(3,i+1):
            #Past growth of i-th day
            str_column_name_2 = "Previous"+str(j)+"AdjustedClose"
            str_column_name_1 = "Previous"+str(j-1)+"AdjustedClose"
            str_name_past = "Past"+ str(j) + "DayGrowth"
            df[str_name_past] = (df[str_column_name_1] - df[str_column_name_2]) / df[str_column_name_2]
            df[str_name_tot] = df[str_name_tot] + df[str_name_past]
        #df["TotPast3DaysGrowth"] = (df["AdjustedClose"] - df["Previous3AdjustedClose"]) / df["Previous3AdjustedClose"]
        #df["TotPast4DaysGrowth"] = (df["AdjustedClose"] - df["Previous4AdjustedClose"]) / df["Previous4AdjustedClose"]
        #df["Past3DayGrowth"] = (df["Previous2AdjustedClose"] - df["Previous3AdjustedClose"]) / df["Previous3AdjustedClose"]
        #df["Past4DayGrowth"] = (df["Previous3AdjustedClose"] - df["Previous4AdjustedClose"]) / df["Previous4AdjustedClose"]



    #Total growth over previous 1, 2, 3 & 4 weeks
    df["TotPastWeekGrowth"] = (df["AdjustedClose"] - df["Previous5AdjustedClose"]) / df["Previous5AdjustedClose"]
    #df["TotPast2WeeksGrowth"] = (df["AdjustedClose"] - df["Previous10AdjustedClose"]) / df["Previous10AdjustedClose"]
    #df["TotPast3WeeksGrowth"] = (df["AdjustedClose"] - df["Previous15AdjustedClose"]) / df["Previous15AdjustedClose"]
    #df["TotPast4WeeksGrowth"] = (df["AdjustedClose"] - df["Previous20AdjustedClose"]) / df["Previous20AdjustedClose"]

    ## 3.3 ANALYSIS 
    # Creates target variable for 2 values of output_split_type 
    df["NextDayDirection"] = (df["NextAdjustedClose"] > df["AdjustedClose"]).astype(int)
    
    #Next 1,2,3,4 day stock growth

    df["NextDayGrowth"] = (df["NextAdjustedClose"] - df["AdjustedClose"]) / df["AdjustedClose"]
    #df["Next2DaysGrowth"] = (df["Next2AdjustedClose"] - df["NextAdjustedClose"]) / df["NextAdjustedClose"]
    #df["Next3DaysGrowth"] = (df["Next3AdjustedClose"] - df["Next2AdjustedClose"]) / df["Next2AdjustedClose"]
    #df["Next4DaysGrowth"] = (df["Next4AdjustedClose"] - df["Next3AdjustedClose"]) / df["Next3AdjustedClose"]

    #Next 1,2,3,4 week stock growth
    #df["NextWeekGrowth"] = (df["Next5AdjustedClose"] - df["AdjustedClose"]) / df["AdjustedClose"]
    #df["Next2WeekGrowth"] = (df["Next10AdjustedClose"] - df["Next5AdjustedClose"]) / df["Next5AdjustedClose"]
    #df["Next3WeekGrowth"] = (df["Next15AdjustedClose"] - df["Next10AdjustedClose"]) / df["Next10AdjustedClose"]
    #df["Next4WeekGrowth"] = (df["Next20AdjustedClose"] - df["Next15AdjustedClose"]) / df["Next15AdjustedClose"]

    return df


