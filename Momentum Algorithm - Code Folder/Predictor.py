#This code contains the class that determines the algorithm to be chosen on a specific day and buys the stocks chosen by the algortihm
from ast import Try
import Simulator
import matplotlib.pyplot as plt
import numpy as np 
import pandas as pd
class Predictor():
    def __init__(self,df,ar_algorithms, v_roll_period, b_compounding, n_begin_backtest, n_end_backtest):
        self.ar_algorithms = ar_algorithms
        self.df = df
        self.v_roll_period = v_roll_period
        self.b_compounding = b_compounding
        self.v_trading_costs = ar_algorithms[0].v_trading_costs
        self.ar_balance_history = [0] * self.v_roll_period
        self.ar_balance_history_notc = [0] * self.v_roll_period
        self.v_balance = 0
        self.v_balance_notc = 0

        self.ar_alg_chosen = np.array([])

        #Creates a frame for every day
        self.df = self.df.sort_values("BarDate")
        df_days = self.df.groupby("BarDate")

        #Get start date of back_test
        ts_start_timestamp = pd.Timestamp(n_begin_backtest)

        #Gets unique names for every day
        ser_days_names = df_days.groups.keys()

        #Print date out of range when error is thrown
        try:
            self.n_start = list(ser_days_names).index(ts_start_timestamp)
        except ValueError: 
            print("No data from this starting day, data starts at : ", list(ser_days_names)[0], ", also weekend days / holidays not in data")

        #Get end date of backtest
        if(not(n_end_backtest == "now")):
            n_end_backtest += " 00:00:00"
            ts_start_timestamp = pd.Timestamp(self.n_begin_backtest)
            self.n_end = list(ser_days_names).index(ts_start_timestamp)
        else: 
            self.n_end = len(list(ser_days_names))
    
    def plot_backtests(self):
        i=0
        color=["red", "blue", "green", "purple", "grey", "yellow", "black", "pink"]
        plt.figure(figsize=(30,10))
        for algo in (self.ar_algorithms):
            #algo.Simulate()
            plt.plot((algo.get_ar_balance_history_notc()-algo.get_ar_balance_history_notc()[self.v_roll_period])[self.v_roll_period:], color = color[i], label = algo.get_name())
            i+=1
        if(len(self.ar_balance_history) > 0):
            plt.plot(self.ar_balance_history_notc[self.v_roll_period:], color = color[i], label = "SWITCHER")
        plt.ylabel("Return")
        plt.xlabel("Days")
        plt.title("Algorithms")
        plt.legend()
        plt.show()

    def rolling_backtest(self):
        for days in range(self.v_roll_period+1,self.n_end - self.n_start-15,1):
            v_max_balance = -100
            for alg in self.ar_algorithms:
                v_balance_alg = alg.get_ar_balance_history_notc()[days-1] - alg.get_ar_balance_history_notc()[days-1-self.v_roll_period]
            #    plt.plot(alg.get_ar_balance_history_notc()[days-self.v_roll_period:days], label = alg.get_name())
                if(v_balance_alg>v_max_balance):
                    v_max_balance = v_balance_alg
                    v_next_change = (alg.get_ar_balance_history_notc()[days] - alg.get_ar_balance_history_notc()[days-1])
                    o_max_alg = alg
            #plt.legend()
            #plt.show()
            print(o_max_alg.get_name(), " : ", v_next_change)
            self.adjust_balance(v_next_change, o_max_alg)
        return

                

        #except:        
    #Function updates the balance for PnL on a day in the simulation
    def adjust_balance(self, v_balance_change, algorithm_chosen):
        #Adds the daily portfolio return to the balance with trading costs (balance) and the balance without trading costs (balance_notc)
        self.ar_alg_chosen = np.append(self.ar_alg_chosen, algorithm_chosen)
        if self.b_compounding:

            #Adjust current balance
            self.v_balance = self.v_balance * (1 + v_balance_change - self.v_trading_costs)
            self.v_balance_notc = self.v_balance_notc * (1 + v_balance_change)

            #Fills the array with daily portfolio returns
            self.ar_balance_history = np.append(self.ar_balance_history, self.v_balance)
            self.ar_balance_history_notc = np.append(self.ar_balance_history_notc, self.v_balance_notc)

        #Without compounding
        else:
            self.v_balance = self.v_balance + v_balance_change - self.v_trading_costs
            self.v_balance_notc = self.v_balance_notc + v_balance_change

            #Fills the array with daily portfolio returns
            self.ar_balance_history = np.append(self.ar_balance_history, self.v_balance)
            self.ar_balance_history_notc = np.append(self.ar_balance_history_notc, self.v_balance_notc)

        return
        
