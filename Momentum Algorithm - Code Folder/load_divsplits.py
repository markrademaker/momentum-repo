
#Package for file import
import webbrowser
import csv
import json
import glob
import os
import getpass
#Package used to check runtime
import time
#Package used for easy to use mathematical operations
import numpy as np
#Package dataframe manipulations
import pandas as pd

#Package for time/loading management
from time import sleep
from tqdm import tqdm

class load_divsplit:

    def __init__(self, df):
        self.df = df

        #Gets all different Symbol Exchange codes, give "div" as argument for dividends, give "splits" as arguments for splits
        df_grouped = df.groupby('SymbolExchangeCode')
        self.ar_stocks_index = df_grouped.groups.keys()

    def progress(percent, width):
        left = width * percent // 100
        right = width - left
        
        tags = "#" * left
        spaces = " " * right
        percents = f"{percent:.0f}%"
        
        print("\r[", tags, spaces, "]", percents, sep="", end="", flush=True)
        
    def get_api_data(self, str_stock, data_type):

        #Loads data for earnings
        if(data_type == "earnings"):
            print("--- START LOADING : begin earnings loading --- ")

            str_format = "csv"

            #Creates the first part of URL
            str_url_API = "https://eodhistoricaldata.com/api/calendar/"+ data_type +"?api_token=5ffd9842655256.72432671&fmt=csv&symbols="

            #Adds all stocks to the URL
            for stock in self.ar_stocks_index:
                str_url_API+= (stock+",")

            #Remove last ,
            str_url_API = str_url_API[0:-1]

            #Last part of URL
            str_url_API += "&from=2010-04-01"
                    
            #downloads the CSV file or JSON file
            chrome_path = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"
            webbrowser.register('chrome', None,webbrowser.BackgroundBrowser(chrome_path))
            webbrowser.get('chrome').open_new(str_url_API)

            #Wait until file has been downloaded
            time.sleep(5)

            #Load the CSV file in python
            download_dir = 'C:\\Users\\' + getpass.getuser()+ '\\Downloads\\*.' + str_format                #Directory to downloads folder of current user
            list_of_files = glob.glob(download_dir)                                                          # * means all if need specific format then *.csv
            API_file_directory = max(list_of_files, key=os.path.getctime)
            print("Downloaded file named (variable : API_file): "+ API_file_directory)

            #Read csv file from downloads
            df_API_stock = pd.read_csv(API_file_directory)
            print("--- LOADING COMPLETED : Earnings finished loading ---")

        #Loads data for splits and dividends
        else: 
            str_format = "json"

            #Creates the first part of URL
            str_url_API = "https://eodhistoricaldata.com/api/"+ data_type + "/"+ str_stock +"?api_token=5ffd9842655256.72432671&from=2010-04-01&fmt="+str_format

            df_API_stock = pd.read_json(str_url_API)
            #Adds SymbolExchangeCode to the dataframe
            if(df_API_stock.size > 0):
                df_API_stock.insert(1,"SymbolExchangeCode_API", str_stock)

        return df_API_stock
    #Note: IF DOWNLOADED FILE IS NOT DESIRED FILE, SET time.sleep(x) TO THE ESTIMATED TIME FOR DOWNLOADING FILE (no harm in taking high estimation)

    #This function loops over all stocks and creates a dataframe with the stock splits and dividend
    def get_divsplitearn(self):

        #initialize list
        list_API = []
        list_API2 = []
        #Gets earnings release data of all stocks at once
        df_earnings = self.get_api_data("", "earnings")

        #Loops over all stocks in index and gets data, grow dataframe by using list instead of dataframe (memory efficiency)
        percent = 10
        width = 40
        print("--- START LOADING : begin Dividends & Stock Splits loading --- ")
        for stock in tqdm(self.ar_stocks_index):
            list_API.append(self.get_api_data(stock, "splits"))       #Chose between splits and div as datatype
            list_API2.append(self.get_api_data(stock, "div"))
        print("--- LOADING COMPLETED : Dividends & Stock Splits finished loading ---")
        #Concatenate all seperate stock data into one file
        df_splits = pd.concat(list_API)
        df_div = pd.concat(list_API2)
        return df_earnings, df_splits, df_div

    #Merges the df with df_API and creates an indicator from df_API
    def f_indicator_creator(self, df_API, left_on, right_on, column_name):

        #merge
        self.df = pd.merge(self.df, df_API, how="left", left_on=left_on, right_on=right_on)
   
        #Adds an indicator column indicating if there is an earnings announcement on that day for that stock
        ser_date = pd.Series(self.df["date"])
        self.df[column_name] = ser_date.where(ser_date.isnull(), 1)

        #convert NaT to 0
        self.df[column_name] = self.df[column_name].where(self.df[column_name]==1, 0)

        self.df[column_name + "_NextDay"] = 0

        #Creates next day div/split/earnings
        for stock in self.ar_stocks_index:
            #Create lag on indicator of a stock
            self.df["temp"] = self.df[self.df["SymbolExchangeCode"]==stock][column_name].shift(-1)
            #Replace NaN with 0
            self.df["temp"] = self.df["temp"].fillna(0)
            #Move lag indicator of stock in column for all stocks
            self.df[column_name + "_NextDay"] = self.df[column_name + "_NextDay"] + self.df["temp"]

        #drops the columns we do not need
        self.df = self.df.drop(columns = df_API)

        return self.df

    #Adds three columns to the dataframe indicating Earnings announcements, dividends and splits
    # SQL join function
    def load_data(self, df_earnings, df_splits, df_div):
        #Converts the datatype of Report_Date to datetime
        df_earnings['Report_Date'] = df_earnings['Report_Date'].astype('datetime64[ns]')
        #Adds the earnings announcements as an indicator to the dataframe
        df_earnings['date'] = df_earnings['Report_Date']
        self.df = self.f_indicator_creator( df_API = df_earnings[["Code", 'date']], left_on=["BarDate", "SymbolExchangeCode"], right_on=["date", "Code"], column_name="Earnings_announcement_ind")

        #Adds the dividend as an indicator to the dataframe
        self.df = self.f_indicator_creator(df_API = df_div[["SymbolExchangeCode_API", 'date']], left_on=["BarDate", "SymbolExchangeCode"], right_on=["date", "SymbolExchangeCode_API"], column_name="dividend_ind")

        #Adds the split as an indicator to the dataframe
        self.df = self.f_indicator_creator(df_API = df_splits[['date', "SymbolExchangeCode_API"]], left_on=["BarDate", "SymbolExchangeCode"], right_on=["date", "SymbolExchangeCode_API"], column_name="split_ind")

        return self.df


